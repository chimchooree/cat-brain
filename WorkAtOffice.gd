extends "res://State.gd"

var scene = load("res://WorkAtOffice.tscn")
var ReceivePraiseAndGlory = load("res://ReceivePraiseAndGlory.tscn")
var BeBad = load("res://BeBad.tscn")

func GoingCrazy(agent):
	if agent.tedium >= 4:
		return true
	else:
		return false

func ReadyForPromotion(agent):
	if agent.reputation % 5 == 0:
		return true
	else:
		return false

func Enter(agent):
	if agent.location != "office":
		print(agent.display_name + " returns to the office.")
		agent.set_location("office")
	print (agent.display_name + ": Time to get to work!")

func Execute(agent):
	agent.set_reputation(agent.reputation + 1)
	agent.set_fatigue(agent.fatigue + 1)
	agent.set_tedium(agent.tedium + 1)
	print ("Hey! " + agent.display_name + " really does the work.")
	if ReadyForPromotion(agent):
		agent.state_machine.ChangeState(ReceivePraiseAndGlory.instance())
	if GoingCrazy(agent):
		agent.state_machine.ChangeState(BeBad.instance())

func Exit(agent):
	print(agent.display_name + ": Time for a break!")