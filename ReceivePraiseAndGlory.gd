extends "res://State.gd"

var scene = load("res://ReceivePraiseAndGlory.tscn")
var CatNap = load("res://CatNap.tscn")

func Enter(agent):
	if agent.location != "office":
		print(agent.display_name + " receives a call from the boss.")
	print (agent.display_name + ": Have I been a good boy?")

func Execute(agent):
	agent.set_position(agent.position + 1)
	agent.set_reputation(agent.reputation + 1)
	agent.set_tedium(agent.tedium - 3)
	print ("Boss: Such a good boy, " + agent.display_name + ".")
	agent.state_machine.ChangeState(CatNap.instance())

func Exit(agent):
	print(agent.display_name +" closes his eyes and sucks up every last bit of attention.")