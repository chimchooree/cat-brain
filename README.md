# Cat Brain

Following along with Mat Buckland's examples in Programming Game AI by Example, 
Chapter 2: State-Driven Agent Design.

He builds the state machine "West World" in C, brick-by-brick with explanations. 
I experiment with applying his techniques and patterns in Godot Engine to a 
state machine of my own. 

## The Program

Cat Brain is basic text-based sim with characters that flow among different 
states based on different conditions. 

**Branch: State Design Pattern**

Cat Man and Dog Man transition from state to state, outputting messages. They 
don't send or receive messages to each other.

## Characters

**Cat Man** is a cat who is also a man who works in the office. He is a diligent 
worker who does it all for the praise and attention. Sometimes, he gets a little 
zany being cooped up in a cubicle for 8 hours and acts out. He also doesn't 
listen to his body, staying awake until the absolute last minute, collapsing 
dramatically into an emergency nap. 

**Dog Man** is a dog who is also a man. He's a happy but distracting friend in 
the office. Unfortunately, he has the bad habit of breathing very audibly, which 
gives him foul breath. It's annoying but not worth hurting his feelings over. 
When he's feeling extra good, his tail starts wagging wildly like the blades on 
a helicopter. His coworkers can't leave anything loose on their desks or it'll 
go flying off.
