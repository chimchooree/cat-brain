extends "res://BaseGameEntity.gd"

# The Cat Man

var display_name = "Cat Man"
var location setget set_location
var reputation = 0 setget set_reputation
var position = 0 setget set_position
var tedium = 0 setget set_tedium
var fatigue = 0 setget set_fatigue
var state_machine_scene = load("res://StateMachine.tscn")
var state_machine
var _timer = null
var initialState = load("res://WorkAtOffice.tscn")

func set_location(newVal):
	location = newVal
func set_reputation(newVal):
	reputation = newVal
func set_position(newVal):
	position = newVal
func set_tedium(newVal):
	tedium = newVal
	if tedium < 0:
		tedium = 0
func set_fatigue(newVal):
	fatigue = newVal

func make_timer():
	_timer = Timer.new()
	add_child(_timer)
	_timer.connect("timeout", self, "_on_Timer_timeout")
	_timer.set_wait_time(1.0)
	_timer.set_one_shot(false) # Make sure it loops
	_timer.start()

func _on_Timer_timeout():
	state_machine.Update()

func make_state_machine():
	state_machine = state_machine_scene.instance()
	state_machine.agent = self
	add_child(state_machine)

func _ready():
	make_state_machine()
	make_timer()