extends "res://State.gd"

var scene = load("res://CatNap.tscn")
var WorkAtOffice = load("res://WorkAtOffice.tscn")

func Enter(agent):
	if agent.location != "home":
		print(agent.display_name + "'s energy is critically low.")
		print(agent.display_name + " screams then runs home for an emergency nap.")
		agent.set_location("home")
		print (agent.display_name + " bounds up to the bed and crashes, asleep before hitting the pillows.")
	else: 
		print (agent.display_name + " sleeps, nestled up the blanket.")

func Execute(agent):
	agent.set_fatigue(agent.fatigue - 1)
	agent.set_tedium(agent.tedium - 1)
	print (agent.display_name + ": zzz...")
	if agent.fatigue < 0:
		agent.state_machine.ChangeState(WorkAtOffice.instance())

func Exit(agent):
	print(agent.display_name +" opens one eye and looks around.")
	print("Good morning, " + agent.display_name + "!")