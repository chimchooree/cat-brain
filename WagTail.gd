extends "res://State.gd"

var scene = load("res://WagTail.tscn")
var BreatheAudibly = load("res://BreatheAudibly.tscn")
var rng = RandomNumberGenerator.new()
var number

func Enter(agent):
	print (agent.display_name + " grins widely. His tail s a-wagging.")

func Execute(agent):
	rng.randomize()
	number = rng.randi_range(0,2)
	if number == 0:
		print(agent.display_name + "'s tail knocks a coworker's pencil jar off his desk.")
	if number == 1:
		print(agent.display_name + "'s tail keeps whacking the blinds.")
	if number == 2:
		print(agent.display_name + "'s tail sweeps some papers off his desk.")
	agent.state_machine.RevertState()

func Exit(agent):
	print(agent.display_name + " frowns.")