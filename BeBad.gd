extends "res://State.gd"

var scene = load("res://BeBad.tscn")
var WorkAtOffice = load("res://WorkAtOffice.tscn")
var huntingStage = 0

func theHunt(agent):
	if huntingStage == 0:
		print (agent.display_name + " whips out his binoculars! Zoom in!!")
	if huntingStage == 1:
		print (agent.display_name + " wiggles his butt a little.")
	if huntingStage == 2:
		print (agent.display_name + " thinks about it.")
	if huntingStage == 3:
		print ("POUNCE! That little piece of paper is no more.")
		huntingStage = 0
	if huntingStage >= 4:
		print (agent.display_name + " screams as he punches his coworker in the face.")
		agent.set_reputation(agent.reputation - 1)
		print ("Coworker: --Hey! Be sweet.")
		huntingStage = 0
	huntingStage += 1

func Enter(agent):
	print(agent.display_name + ": Everything is so boring!")
	print(agent.display_name + "'s going crazy!!")

func Execute(agent):
	agent.set_tedium(agent.tedium - 1)
	theHunt(agent)
	if agent.tedium <= 0:
		agent.state_machine.ChangeState(WorkAtOffice.instance())

func Exit(agent):
	print(agent.display_name + " licks his hand then slicks back his hair.")
	print(agent.display_name + ": Who, me? I wasn't doing anything.")