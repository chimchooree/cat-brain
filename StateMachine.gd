extends Node

var agent
var current_state
var previous_state
var global_state_scene = load("res://GlobalState.tscn")
var global_state
var setup = true

func Update():
	if global_state:
		global_state.Execute(agent)
	if current_state:
		current_state.Execute(agent)

func ChangeState(newState):
	if current_state and newState:
		current_state.Exit(agent)
		previous_state = current_state.scene
		current_state.queue_free()
		current_state = newState
		current_state.Enter(agent)
	if setup and newState:
		setup = false
		current_state = newState
		current_state.Enter(agent)

func RevertState():
	ChangeState(previous_state.instance())

func isInState(state):
	# current is an instance and state is more general, so difficult to compare
	return current_state == state;

func _ready():
	global_state = global_state_scene.instance()
	ChangeState(agent.initialState.instance())