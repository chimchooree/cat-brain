extends "res://BaseGameEntity.gd"

# The Dog Man

var display_name = "Dog Man"
var location setget set_location
var state_machine_scene = load("res://StateMachine.tscn")
var state_machine
var _timer = null
var initialState = load("res://BreatheAudibly.tscn")

func set_location(newVal):
	location = newVal

func make_timer():
	_timer = Timer.new()
	add_child(_timer)
	_timer.connect("timeout", self, "_on_Timer_timeout")
	_timer.set_wait_time(2.5)
	_timer.set_one_shot(false) # Make sure it loops
	_timer.start()

func _on_Timer_timeout():
	state_machine.Update()

func make_state_machine():
	state_machine = state_machine_scene.instance()
	state_machine.agent = self
	add_child(state_machine)

func _ready():
	make_state_machine()
	make_timer()