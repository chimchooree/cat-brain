extends "res://State.gd"

var scene = load("res://BreatheAudibly.tscn")
var WagTail = load("res://WagTail.tscn")
var rng = RandomNumberGenerator.new()
var number

func Enter(agent):
	print (agent.display_name + " begins breathing audibly.")

func Execute(agent):
	rng.randomize()
	if rng.randf() <= 0.05:
		agent.state_machine.ChangeState(WagTail.instance())
	number = rng.randi_range(0,2)
	if number == 0:
		print(agent.display_name + " huffs and puffs.")
	if number == 1:
		print(agent.display_name + " wheezes wetly.")
	if number == 2:
		print(agent.display_name + "'s tongue lulls out of his mouth.")

func Exit(agent):
	print(agent.display_name + " hushes suddenly.")